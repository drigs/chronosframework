﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace ChronosFramework.FirstPersonController
{
    public class FirstPersonController : Singleton<FirstPersonController>
    {
        #region References

        [SerializeField]
        AudioClip[] footsteps;
        [SerializeField]
        LayerMask interactionMask;
        [SerializeField]
        Image crosshair;
        #endregion

        #region Attributes

        public float radius { get; private set; }
        public float height { get; private set; }
        public float speed = 5f;
        public float runSpeed = 10f;
        public float turnSpeed = 2f;
        public float interactionDistance = 5f;
        public float zoomAmount = 25;
                
        float initialFov;
        float initialCamHeight;
        float stepCycle;
        float nextStep;

        bool canMove = true;
        bool canRotate = true;
        bool canUnlock = true;

        bool zooming;
        bool running;
        bool crouching;

        CharacterController cc;

        BoxCollider boxChecker;

        AudioSource audioSource;

        Action specialAction;
        Action onActionEnd;

        Vector2 crosshairSize;
        Vector2 currentCrosshairSize;
        #endregion

        #region Methods
        void Initialize()
        {
            audioSource = GetComponent<AudioSource>();

            cc = GetComponent<CharacterController>();

            radius = cc.radius;
            height = cc.height;

            initialFov = Camera.main.fieldOfView;

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;

            crosshair.color = Color.gray;

            currentCrosshairSize = crosshairSize = crosshair.rectTransform.sizeDelta;

            TweakCrosshairSize(Vector2.one);
        }

        void Awake()
        {
            Initialize();
        }

        void FixedUpdate()
        {
            if (cc.enabled)
                cc.Move(Physics.gravity * Time.fixedDeltaTime);

            if (canMove)
            {
                //Movement
                float xMove = Input.GetAxisRaw("Horizontal");
                float zMove = Input.GetAxisRaw("Vertical");

                if (Mathf.Abs(xMove) > 0 || Mathf.Abs(zMove) > 0)
                {
                    Vector3 movement = (transform.right * xMove + transform.forward * zMove).normalized;

                    RaycastHit hitInfo;
                    Physics.SphereCast(transform.position, cc.radius * 2, Vector3.down, out hitInfo, cc.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);

                    movement = Vector3.ProjectOnPlane(movement, hitInfo.normal).normalized;

                    Vector3 direction = movement.normalized;

                    movement = direction * (running ? runSpeed : (crouching ? speed / 2 : speed)) * Time.fixedDeltaTime;
                    movement.y -= .1f;

                    Walk(movement);
                }
            }
        }

        void Update()
        {
            if (canRotate)
            {
                //Rotation
                float xRot = Input.GetAxis("Mouse Y") * turnSpeed;
                float yRot = Input.GetAxis("Mouse X") * turnSpeed;

                Quaternion camRot0 = Camera.main.transform.localRotation;
                Quaternion playerRot0 = transform.localRotation;

                Quaternion cameraRot = Camera.main.transform.localRotation * Quaternion.Euler(-xRot, 0, 0);
                Quaternion playerRot = transform.localRotation * Quaternion.Euler(0, yRot, 0);

                cameraRot = ClampRotationAroundXAxis(cameraRot);

                transform.localRotation = playerRot;
                Camera.main.transform.localRotation = cameraRot;
            }

            CheckInput();

            if (specialAction != null)
            {
                specialAction();
            }
        }

        public void SetLock(bool value)
        {
            canUnlock = !value;
        }

        public void SetMove(bool value)
        {
            cc.enabled = canMove = value;
        }

        public void SetRotate(bool value)
        {
            canRotate = value;
        }

        void Walk(Vector3 movement)
        {
            Vector3 previousPos = transform.position;

            cc.Move(movement);

            stepCycle += (cc.velocity.magnitude + speed) * Time.fixedDeltaTime;

            if (Vector3.Distance(previousPos, transform.position) > .05f)
            {
                if (!(stepCycle > nextStep))
                {
                    return;
                }

                nextStep = stepCycle + 3.5f;

                int n = UnityEngine.Random.Range(1, footsteps.Length);

                audioSource.clip = footsteps[n];
                audioSource.pitch = UnityEngine.Random.Range(.85f, 1.15f);
                audioSource.PlayOneShot(audioSource.clip);

                footsteps[n] = footsteps[0];
                footsteps[0] = audioSource.clip;
            }
        }

        Quaternion ClampRotationAroundXAxis(Quaternion q)
        {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

            angleX = Mathf.Clamp(angleX, -60, 60);

            q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

            return q;
        }

        void CheckInput()
        {

            if (!Input.GetButton("Fire2"))
            {
                if (zooming)
                {
                    Tween cameraZoom = new Tween(Camera.main.fieldOfView, initialFov, .25f, false, EasingType.EaseOut, delegate (float value)
                    {
                        Camera.main.fieldOfView = value;
                    });

                    zooming = false;
                }
            }
            else
            {
                if (!zooming)
                {
                    Tween cameraZoom = new Tween(Camera.main.fieldOfView, initialFov - 25, .25f, false, EasingType.EaseOut, delegate (float value)
                    {
                        Camera.main.fieldOfView = value;
                    });

                    zooming = true;
                }
            }

            if (Input.GetButtonDown("Fire1"))
            {
                if (canUnlock)
                {
                    if (onActionEnd != null)
                    {
                        onActionEnd();
                        onActionEnd = null;
                    }

                    specialAction = null;
                }
            }
        }

        public bool CheckInteractionDistance(Vector3 target)
        {
            return Vector3.Distance(transform.position, target) <= interactionDistance;
        }

        public void TweakCrosshairSize(Vector2 newSize)
        {
            if (currentCrosshairSize != newSize)
            {
                currentCrosshairSize = newSize;

                newSize = new Vector2(crosshairSize.x * newSize.x, crosshairSize.y * newSize.y);

                Tween sizeTween = new Tween(crosshair.rectTransform.sizeDelta, newSize, .2f, false, EasingType.EaseOut, delegate (Vector3 size)
                {
                    crosshair.rectTransform.sizeDelta = size;
                });

                if (newSize.x > crosshairSize.x * 2)
                {
                    Tween colorTween = new Tween(crosshair.color, Color.white, .2f, false, EasingType.EaseOut, delegate (Color color)
                    {
                        crosshair.color = color;
                    });
                }
                else
                {
                    Tween colorTween = new Tween(crosshair.color, Color.gray, .2f, false, EasingType.EaseOut, delegate (Color color)
                    {
                        crosshair.color = color;
                    });
                }
            }
        }
        #endregion
    }
}