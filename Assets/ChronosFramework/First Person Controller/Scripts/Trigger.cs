﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;

namespace ChronosFramework.FirstPersonController
{
    public class Trigger : Highlightable
    {
        public bool repeatable;
        public UnityEvent onClick;
        public UnityEvent onCollision;

        public override void OnMouseDown()
        {
            if (isHighlighted)
            {
                if (onClick != null)
                {
                    onClick.Invoke();

                    if (!repeatable)
                    {
                        Destroy(this);
                    }
                }
            }
        }

        public override void OnMouseEnter()
        {
            if (onClick != null)
            {
                base.OnMouseEnter();
            }
        }

        void OnTriggerEnter(Collider collider)
        {
            if (onCollision != null)
            {
                onCollision.Invoke();

                if (!repeatable)
                {
                    onCollision = null;
                }
            }
        }

        void OnCollisionEnter(Collision collision)
        {
            if (onCollision != null)
            {
                onCollision.Invoke();

                if (!repeatable)
                {
                    onCollision = null;
                }
            }
        }
    }
}