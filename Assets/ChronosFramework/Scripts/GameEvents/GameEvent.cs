﻿using System;
using UnityEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ChronosFramework.EventSystem
{
    public class GameEvent
    {
        public Func<bool> triggerCondition { get; set; }
        public UpdateAction onTrigger { get; set; }
        
        public GameEvent(){}

        public GameEvent(UpdateAction onTrigger, Func<bool> triggerCondition)
        {
            this.triggerCondition = triggerCondition;
            this.onTrigger = onTrigger;
        }
    }
}
