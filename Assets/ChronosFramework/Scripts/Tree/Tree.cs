﻿using UnityEngine;
using System.Collections;

public class Tree : MonoBehaviour
{
    Node root = new Node(10, 2);
    int c;

    void Awake()
    {
        root.Instantiate();
        root.go.name = "Root";
    }

    void Update()
    {
        for (int i = 0; i < 10; i++)
        {
            if (Input.GetKeyDown((KeyCode)49 + i))
            {
                c++;

                Node newNode = new Node(i + 1, 2);

                if (root.AddChild(newNode))
                {                    
                    Debug.Log("Added Node " + c + " to Node " + newNode.parent.value);
                }
                else
                {
                    Debug.Log("Value too high!");
                }
            }
        }
    }
}
