﻿using UnityEngine;
using System.Linq;
using System;

public class Node
{
    public GameObject go;

    public int maxNumberOfChildren;

    public int value { get; private set; }

    public bool IsFull { get { return children.Length == maxNumberOfChildren; } }

    public bool IsEmpty { get { return children.Length == 0; } }

    public Node parent { get; private set; }

    Node[] children = new Node[0];
    
    public Node(int value, int maxNumberOfChildren = int.MaxValue)
    {
        this.value = value;
        this.maxNumberOfChildren = maxNumberOfChildren;
    }

    public void Instantiate()
    {
        go = GameObject.CreatePrimitive(PrimitiveType.Cube);
        go.name = value.ToString();

        if (parent != null)
            go.transform.SetParent(parent.go.transform);
    }

    public bool AddChild(Node child)
    {
        if (!IsFull)
        {
            child.parent = this;

            Array.Resize(ref children, children.Length + 1);
            children[children.Length - 1] = child;

            children.OrderBy(c => c.value);
            
            child.Instantiate();
            
            if (go.transform.childCount > 1)
            {
                var child0 = go.transform.GetChild(0);
                var child1 = go.transform.GetChild(1);

                if (int.Parse(child0.name) > int.Parse(child1.name))
                {
                    child0.SetAsLastSibling();
                }
            }

            return true;
        }
        else
        {
            foreach (var myChild in children)
            {
                if (child.value < myChild.value)
                {
                    if (myChild.AddChild(child))
                    {
                        return true;
                    }
                }
            }

            var mostEmpty = children[0];

            foreach (var myChild in children)
            {
                if (myChild.children.Length < mostEmpty.children.Length)
                {
                    mostEmpty = myChild;
                }
            }

            return mostEmpty.AddChild(child);
        }
    }
}
