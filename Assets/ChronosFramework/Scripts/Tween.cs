﻿using UnityEngine;
using System;

namespace ChronosFramework
{
    /// <summary>
    /// Used for interpolaton.
    /// </summary>
    public class Tween
    {
        public event Action OnComplete;

        UpdateAction Interpolate;

        float elapsedTime;
        float duration;

        bool isFixed;

        public bool isLoop { get; set; }
        public bool isReverse { get; private set; }

        /// <summary>
        /// Interpolates values from <paramref name="startValue"/> to <paramref name="endValue"/> and passes them to <paramref name="Callback"/>.
        /// </summary>
        public Tween(float startValue, float endValue, float duration, bool isFixed, EasingType easing, Action<float> Callback)
        {
            float value = startValue;

            this.isFixed = isFixed;
            this.duration = duration;

            AnimationCurve curve = easing.GetAnimationCurve();

            Interpolate = delegate
            {
                value = startValue + ((endValue - startValue) * curve.Evaluate(elapsedTime / duration));

                Callback(value);
            };

            AddTween();
        }

        /// <summary>
        /// Interpolates points from <paramref name="startValue"/> to <paramref name="endValue"/> and passes them to <paramref name="Callback"/>.
        /// </summary>
        public Tween(Vector3 startValue, Vector3 endValue, float duration, bool isFixed, EasingType easing, Action<Vector3> Callback)
        {
            Vector3 value = startValue;

            this.isFixed = isFixed;
            this.duration = duration;

            AnimationCurve curve = easing.GetAnimationCurve();
            
            Interpolate = delegate
            {
                value = startValue + ((endValue - startValue) * curve.Evaluate(elapsedTime / duration));

                Callback(value);
            };

            AddTween();
        }

        /// <summary>
        /// Interpolates colors from <paramref name="startValue"/> to <paramref name="endValue"/> and passes them to <paramref name="Callback"/>.
        /// </summary>
        public Tween(Color startValue, Color endValue, float duration, bool isFixed, EasingType easing, Action<Color> Callback)
        {
            Color value = startValue;

            this.isFixed = isFixed;
            this.duration = duration;

            AnimationCurve curve = easing.GetAnimationCurve();

            Interpolate = delegate
            {
                value = startValue + ((endValue - startValue) * curve.Evaluate(elapsedTime / duration));

                Callback(value);
            };

            AddTween();
        }

        /// <summary>
        /// Interpolates rotation from <paramref name="startValue"/> to <paramref name="endValue"/> and passes them to <paramref name="Callback"/>.
        /// </summary>
        public Tween(Quaternion startValue, Quaternion endValue, float duration, bool isFixed, EasingType easing, Action<Quaternion> Callback)
        {
            this.isFixed = isFixed;
            this.duration = duration;

            Quaternion value = startValue;

            AnimationCurve curve = easing.GetAnimationCurve();

            Interpolate = delegate
            {
                value.x = startValue.x + ((endValue.x - startValue.x) * curve.Evaluate(elapsedTime / duration));
                value.y = startValue.y + ((endValue.y - startValue.y) * curve.Evaluate(elapsedTime / duration));
                value.z = startValue.z + ((endValue.z - startValue.z) * curve.Evaluate(elapsedTime / duration));
                value.w = startValue.w + ((endValue.w - startValue.w) * curve.Evaluate(elapsedTime / duration));

                Callback(value);
            };

            AddTween();
        }

        /// <summary>
        /// Interpolates position and rotation values from <paramref name="start"/> to <paramref name="end"/> and passes them to <paramref name="Callback"/>.
        /// </summary>
        public Tween(Transform start, Transform end, float duration, bool isFixed, EasingType easing, Action<Vector3, Quaternion> Callback)
        {
            this.isFixed = isFixed;
            this.duration = duration;

            Vector3 initialPos = start.position;
            Quaternion initialRot = start.rotation;

            Vector3 pos = start.position;
            Quaternion rot = start.rotation;

            AnimationCurve curve = easing.GetAnimationCurve();

            Interpolate = delegate
            {
                rot.x = initialRot.x + ((end.rotation.x - initialRot.x) * curve.Evaluate(elapsedTime / duration));
                rot.y = initialRot.y + ((end.rotation.y - initialRot.y) * curve.Evaluate(elapsedTime / duration));
                rot.z = initialRot.z + ((end.rotation.z - initialRot.z) * curve.Evaluate(elapsedTime / duration));
                rot.w = initialRot.w + ((end.rotation.w - initialRot.w) * curve.Evaluate(elapsedTime / duration));

                pos = initialPos + ((end.position - initialPos) * curve.Evaluate(elapsedTime / duration));

                Callback(pos, rot);
            };

            AddTween();
        }

        /// <summary>
        /// Interpolates points in <paramref name="spline"/> and passes them to <paramref name="Callback"/>.
        /// </summary>
        public Tween(Spline spline, float duration, bool isFixed, Action<Vector3> Callback)
        {
            this.isFixed = isFixed;
            this.duration = duration;

            isLoop = spline.Loop;

            Interpolate = delegate
            {
                Callback(spline.GetPoint(elapsedTime / duration));
            };

            AddTween();
        }

        /// <summary>
        /// Interpolates <paramref name="target"/> through points in <paramref name="spline"/>.
        /// </summary>
        public Tween(Transform target, Spline spline, float duration, bool isFixed, bool lookForward, bool isReverse = false)
        {
            this.isFixed = isFixed;
            this.duration = duration;
            this.isReverse = isReverse;

            isLoop = spline.Loop;

            float t = 0;
            Vector3 position = Vector3.zero;

            Interpolate = delegate
            {
                t = elapsedTime / duration;
                position = spline.GetPoint(t);

                target.position = position;                
            };

            if (lookForward)
            {
                Interpolate += delegate
                {
                    Vector3 direction = Vector3.zero;

                    if (isReverse)
                    {
                        direction = -spline.GetDirection(1 - t);
                    }
                    else
                    {
                        direction = spline.GetDirection(t);
                    }

                    target.LookAt(position + direction);
                };
            }

            AddTween();
        }

        public void Start()
        {
            if (isFixed)
            {
                Interpolate.AddToFixedUpdate();
            }
            else
            {
                Interpolate.AddToUpdate();
            }
        }

        public void Pause()
        {
            if (isFixed)
            {
                Interpolate.RemoveFromFixedUpdate();
            }
            else
            {
                Interpolate.RemoveFromUpdate();
            }
        }

        public void Stop()
        {
            if (isFixed)
            {
                Interpolate.RemoveFromFixedUpdate();

                elapsedTime = 0;
            }
            else
            {
                Interpolate.RemoveFromUpdate();

                elapsedTime = 0;
            }
        }

        private void AddTween()
        {
            if (isReverse)
            {
                elapsedTime = duration;
            }

            if (isFixed)
            {
                if (isReverse)
                {
                    Interpolate += delegate
                    {
                        if (elapsedTime > 0)
                        {
                            elapsedTime -= Time.fixedDeltaTime;
                        }
                        else
                        {
                            elapsedTime = 0;

                            Complete();

                            if (isLoop)
                            {
                                elapsedTime = duration - Time.fixedDeltaTime;
                            }
                            else
                            {
                                Interpolate.RemoveFromFixedUpdate();
                            }
                        }
                    };
                }
                else
                {
                    Interpolate += delegate
                    {
                        if (elapsedTime < duration)
                        {
                            elapsedTime += Time.fixedDeltaTime;
                        }
                        else
                        {
                            elapsedTime = duration;

                            Complete();

                            if (isLoop)
                            {
                                elapsedTime = Time.fixedDeltaTime;
                            }
                            else
                            {
                                Interpolate.RemoveFromFixedUpdate();
                            }
                        }
                    };
                }

                Interpolate.AddToFixedUpdate();
            }
            else
            {
                if (isReverse)
                {
                    Interpolate += delegate
                    {
                        if (elapsedTime > 0)
                        {
                            elapsedTime -= Time.deltaTime;
                        }
                        else
                        {
                            elapsedTime = 0;

                            Complete();

                            if (isLoop)
                            {
                                elapsedTime = duration - Time.deltaTime;
                            }
                            else
                            {
                                Interpolate.RemoveFromUpdate();
                            }
                        }
                    };
                }
                else
                {
                    Interpolate += delegate
                    {
                        if (elapsedTime < duration)
                        {
                            elapsedTime += Time.deltaTime;
                        }
                        else
                        {
                            elapsedTime = duration;

                            Complete();

                            if (isLoop)
                            {
                                elapsedTime = Time.deltaTime;
                            }
                            else
                            {
                                Interpolate.RemoveFromUpdate();
                            }
                        }
                    };
                }

                Interpolate.AddToUpdate();
            }
        }

        private void Complete()
        {
            if (OnComplete != null)
            {
                OnComplete();
            }
        }
    }
}