﻿using System;

namespace ChronosFramework
{
    static class EventManager
    {
        public static void AddToUpdate(this UpdateAction action)
        {
            CoreManager.Instance.OnUpdate += action;
        }

        public static void While(this UpdateAction action, Func<bool> condition)
        {
            UpdateAction updateAction = null;
            updateAction = delegate
            {
                if (condition())
                {
                    action();
                }
                else
                {
                    updateAction.RemoveFromUpdate();
                }
            };

            CoreManager.Instance.OnUpdate += updateAction;
        }

        public static void AddToFixedUpdate(this UpdateAction action)
        {
            CoreManager.Instance.OnFixedUpdate += action;
        }

        public static void WhileFixed(this UpdateAction action, Func<bool> condition)
        {
            UpdateAction updateAction = null;
            updateAction = delegate
            {
                if (condition())
                {
                    action();
                }
                else
                {
                    updateAction.RemoveFromFixedUpdate();
                }
            };

            CoreManager.Instance.OnFixedUpdate += updateAction;
        }

        public static void RemoveFromUpdate(this UpdateAction action)
        {
            CoreManager.Instance.OnUpdate -= action;
        }

        public static void RemoveFromFixedUpdate(this UpdateAction action)
        {
            CoreManager.Instance.OnFixedUpdate -= action;
        }
    }
}
