﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ChronosFramework
{
    public class SplineHandle : MonoBehaviour
    {
        public enum Axis
        {
            X, Y, Z
        }

        public Spline spline;
        public Transform target;
        public Axis axis;
        public float speed;
        float lastAxisPos;
        float splinePoint;

        void Start()
        {
            float axisMovement = 0;
            switch (axis)
            {
                case Axis.X:
                    axisMovement = transform.position.x;
                    lastAxisPos = spline.GetPoint(0).x;
                    break;
                case Axis.Y:
                    axisMovement = transform.position.y;
                    lastAxisPos = spline.GetPoint(0).y;
                    break;
                case Axis.Z:
                    axisMovement = transform.position.z;
                    lastAxisPos = spline.GetPoint(0).z;
                    break;
            }

            UpdateAction Update = null;
            Update = () =>
            {
                switch (axis)
                {
                    case Axis.X:
                        axisMovement = transform.position.x;
                        break;
                    case Axis.Y:
                        axisMovement = transform.position.y;
                        break;
                    case Axis.Z:
                        axisMovement = transform.position.z;
                        break;
                }

                if (lastAxisPos != axisMovement)
                {
                    splinePoint += (axisMovement - lastAxisPos) / (1 / speed) * Time.fixedDeltaTime;
                    //splinePoint /= spline.ControlPointCount;

                    target.transform.position = spline.GetPoint(splinePoint);
                    lastAxisPos = axisMovement;

                    //Debug.Log(splinePoint);
                }
            };

            Update.AddToFixedUpdate();
        }
    }
}