﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class CameraLook : MonoBehaviour {

    public Transform target;
    public UpdateAction Look { get; private set; }

    void Start()
    {
        Look = () =>
        {
            transform.LookAt(target);
        };

        Look.AddToUpdate();
    }
}
