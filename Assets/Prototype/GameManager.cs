﻿using UnityEngine;
using ChronosFramework;

using System.Collections;
using ChronosFramework.EventSystem;

public class GameManager : Singleton<GameManager>
{
    [SerializeField]Transform destination;
    [SerializeField]Transform[] destinations;

    GameEventQueue eventGroup = new GameEventQueue();

    void Awake()
    {
        InputManager.AddInputAction(KeyCode.Alpha1, InputType.KeyDown, () => Debug.Log("Pressed 1"));
        InputManager.AddInputAction(KeyCode.Alpha1, InputType.KeyUp, () => Debug.Log("You stopped pressing 1"));

        UpdateAction hold2 = () => Debug.Log("Holding 2");

        InputManager.AddInputAction(KeyCode.Alpha2, InputType.KeyHold, hold2);

        InputManager.AddInputAction(KeyCode.R, InputType.KeyDown, () => InputManager.RemoveInputAction(hold2));
        
        int triggerCount = 0;

        UpdateAction action = delegate
        {
            triggerCount++;
            Debug.Log("Pressed space " + triggerCount + " times");
        };

        InputManager.AddInputAction(KeyCode.Space, InputType.KeyDown, action);

        eventGroup.Enqueue(new GameEvent(() => Debug.Log("Event 1 triggered!"), () => triggerCount >= 3));
        eventGroup.Enqueue(new GameEvent(() => Debug.Log("Event 2 triggered!"), () => triggerCount >= 10));
    }

    public void CompleteEvent()
    {
        if (!eventGroup.TriggerEvent())
        {
            Debug.Log("Conditions not met!");
        }
    }
}
