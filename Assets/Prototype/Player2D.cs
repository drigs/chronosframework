﻿using ChronosFramework;
using System.Collections.Generic;
using UnityEngine;

public class Player2D : MonoBehaviour
{
    new Rigidbody rigidbody;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();

        UpdateAction Update = null;

        Update = () =>
        {
            Vector2 movement = rigidbody.velocity;

            movement.x = Input.GetAxis("Horizontal") * 2.5f;

            rigidbody.velocity = movement;
        };

        Update.AddToUpdate();
    }
}
